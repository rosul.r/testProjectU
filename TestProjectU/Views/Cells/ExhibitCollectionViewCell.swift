//
//  ExhibitCollectionViewCell.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import UIKit

class ExhibitCollectionViewCell: UICollectionViewCell, BaseCellProtocol {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var itemTitleLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        activityIndicator.isHidden = false
        itemTitleLabel.text = ""
    }

}
