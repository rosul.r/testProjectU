//
//  ExhibitTableViewCell.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import UIKit

class ExhibitTableViewCell: UITableViewCell, BaseCellProtocol {
    
    @IBOutlet var collectionView: UICollectionView!
    var exhibitItem: Exhibit? {
        didSet {
            collectionView.reloadData()
            collectionView.contentOffset = .zero
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        exhibitItem = nil
    }
    
}

extension ExhibitTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return exhibitItem?.images.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExhibitCollectionViewCell.identifier(), for: indexPath) as! ExhibitCollectionViewCell
        cell.itemTitleLabel.text = exhibitItem?.title ?? ""
        if let url = exhibitItem?.images[indexPath.item] {
            cell.imageView.downloadFromURL(url, completionHandler: { [weak cell]
                image in
                DispatchQueue.main.async() {
                    cell?.activityIndicator.isHidden = image != nil
                    cell?.imageView.image = image
                }
            })
        }
        return cell
    }
}

extension ExhibitTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dimmention = collectionView.frame.height
        
        return CGSize(width: dimmention, height: dimmention)
    }
}
