//
//  ViewController.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var dataProvider: IExhibitsLoader?
    var exhibitItems: [Exhibit] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()

        dataProvider = FileExhibitsLoader()
        dataProvider?.getExhibitList({ [weak self] (fetchedItems) in
            self?.exhibitItems = fetchedItems
            self?.tableView.reloadData()
        })
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exhibitItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExhibitTableViewCell.identifier(), for: indexPath) as! ExhibitTableViewCell
        cell.selectionStyle = .none
        cell.exhibitItem = exhibitItems[indexPath.item]
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width
    }
}

