//
//  ImageView+URL.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadFromURL(_ url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit, completionHandler: @escaping (UIImage?)->()) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    completionHandler(nil)
                    return
            }
                completionHandler(image)
            }.resume()
    }
}
