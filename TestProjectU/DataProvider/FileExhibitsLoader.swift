//
//  FileExhibitsLoader.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import Foundation

class FileExhibitsLoader: IExhibitsLoader {
    
    private let sourceLink = "https://gist.githubusercontent.com/u-android/41ade05b6ae1133e7e86e9dfd55f1794/raw/bab1c383b0384d1a4c889b399ad7b13ae05634fa/ios%20challenge%20json"
    private let session: URLSession = .shared

    func getExhibitList(_ handler: @escaping ([Exhibit])->Void) {
        guard let url = URL(string: sourceLink) else { return }
        let task = session.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : AnyObject],
                    let itemsArray = json["list"] as? [[String : AnyObject]] {
                    let result = Exhibit.createWith(itemsArray)
                    DispatchQueue.main.async() {
                        handler(result)
                    }
                }
            }
            catch {
                print("Error: \(error)")
            }
        }
        task.resume()
    }
}
