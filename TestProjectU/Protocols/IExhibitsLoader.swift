//
//  IExhibitsLoader.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import Foundation

protocol IExhibitsLoader {
    func getExhibitList(_ handler: @escaping ([Exhibit])->Void)
}
