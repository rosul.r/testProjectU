//
//  BaseCellProtocol.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import UIKit

protocol BaseCellProtocol {
    static func identifier() -> String
}

extension BaseCellProtocol {
    static func identifier() -> String {
        return String(describing:self)
    }
}
