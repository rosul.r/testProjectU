//
//  Exhibit.swift
//  TestProjectU
//
//  Created by Roman Rosul on 21.06.18.
//  Copyright © 2018 Roman Rosul. All rights reserved.
//

import Foundation

class Exhibit {
    
    var title: String?
    var images: [URL] = []

    typealias JSONObject = [String: Any]

    init(dictionary: JSONObject) {
        title = dictionary["title"] as? String
        if let imagesArray = dictionary["images"] as? [String] {
            for link in imagesArray {
                guard let url = URL(string: link) else { continue }
                images.append(url)
            }
        }
    }
    
    static func createWith(_ dictionariesArray: [JSONObject]) -> [Exhibit] {
        var tempArray: [Exhibit] = []
        for dict in dictionariesArray {
             let value = Exhibit(dictionary: dict)
             tempArray.append(value)
        }
        return tempArray
    }

}


